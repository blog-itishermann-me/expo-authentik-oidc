// useAuth.ts
import { useAutoDiscovery, makeRedirectUri, useAuthRequest, revokeAsync } from "expo-auth-session";
import { Alert } from "react-native";
import { useCallback, useEffect, useState } from "react";
import { decodeToken, getToken, type Claims, saveToken, removeToken } from "./token";
import { openBrowserAsync } from "expo-web-browser";

const redirectUri = makeRedirectUri();
const AuthentikClientId = 'your-client-id-here';
const AuthentikAppUrl = 'https:/[Your authentik domain]/application/o/[Your authentik app slug]';

export default function useAuth() {
	const [claims, setClaims] = useState<Claims | null>(null);
	const discovery = useAutoDiscovery(AuthentikAppUrl);

	// To retrieve the redirect URL, add this to the callback URL list
  // of your Authentik application.
  // console.log(`Redirect URL: ${redirectUri}`);

	const [request, result, promptAsync] = useAuthRequest(
    {
      redirectUri,
      clientId: AuthentikClientId,
      // id_token will return a JWT token
      responseType: "id_token",
      // retrieve the user's profile
      scopes: ["openid", "profile"],
      usePKCE: true,
      extraParams: {
        // ideally, this will be a random value
        nonce: "nonce",
      },
    },
    discovery
  );

	const handleResult = useCallback(() => {
    if(!result) return;
    if (result.type === 'error') {
      Alert.alert(
        "Authentication error",
        result.params.error_description || "something went wrong"
      );
      return;
    }
    if (result.type === "success") {
			if(!result.authentication || !result.authentication.idToken) return;
      // Retrieve the JWT token and decode it
      const jwtToken = result.authentication.idToken;
      const decoded = decodeToken(jwtToken);
      setClaims(decoded);
			saveToken(result.authentication);
    }
  }, [result]);

  useEffect(()=> {
		handleResult();
  }, [handleResult]);

	const retrieveTokenOnMount = useCallback(async () => {
		const token = await getToken();
		if (!token || !token.idToken) return;
		if(token.shouldRefresh()){
			await token.refreshAsync({
				...token.getRequestConfig(),
				clientId: AuthentikClientId,
			},
				{
					tokenEndpoint: discovery?.tokenEndpoint
				}
			);
		}
		const decoded = decodeToken(token.idToken);
		setClaims(decoded);
	}, [discovery]);

	useEffect(() => {
		retrieveTokenOnMount();
	}, [retrieveTokenOnMount]);

	const revoke = async () => {
		const token = await getToken();
		if(!token) return;
		await revokeAsync(
			{
				...token.getRequestConfig(),
				token: token.accessToken,
				clientId: AuthentikClientId,
			},
			{
				revocationEndpoint: discovery?.revocationEndpoint,
			}
		);
		setClaims(null);
		await removeToken();
		if(discovery?.endSessionEndpoint) {
			await openBrowserAsync(
				discovery?.endSessionEndpoint
			);
		}
	};

	return { claims, request, promptAsync, revoke };
}
