import { Button, StyleSheet, Text, View } from "react-native";
import useAuth from "./useAuth";
import { Claims } from "./token";

export default function App() {
  const { claims,request, promptAsync,revoke } = useAuth();
  return (
    <View style={styles.container}>
      {claims ? (
        <>
          <Text style={styles.title}>You are logged in!</Text>
          <Text style={styles.title}>JWT claims:</Text>
          {Object.keys(claims).map((key) => (
            <Text key={key}>
              {key}: {claims[key as keyof Claims]}
            </Text>
          ))}
          <Button title="Log out" onPress={revoke} />
        </>
      ) : (
        <Button
          disabled={!request}
          title="Log in"
          onPress={() => promptAsync()}
        />
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  title: {
    fontSize: 20,
    textAlign: "center",
    marginTop: 40,
  },
});
