// token.ts
import { decode } from "base-64";
import * as SecureStore from 'expo-secure-store';
import { jwtDecode, type JwtPayload } from 'jwt-decode';
import { TokenResponse } from "expo-auth-session";

const AUTH_TOKEN_KEY = 'AUTH_TOKEN_KEY';

export async function saveToken(token: TokenResponse) {
  await SecureStore.setItemAsync(AUTH_TOKEN_KEY,JSON.stringify(token));
}

export async function getToken(): Promise<TokenResponse | null> {
	const result = await SecureStore.getItemAsync(AUTH_TOKEN_KEY);
	if (!result) return null;
	return new TokenResponse(JSON.parse(result));
}

export async function removeToken() {
	await SecureStore.deleteItemAsync(AUTH_TOKEN_KEY);
}

export interface Claims extends JwtPayload {
  name: string;
  preferred_username: string;
  email: string;
  nickname: string;
  groups: string[];
  given_name: string;
}

export function decodeToken(token: string) {
	global.atob = decode; // Dangerous overload
	return jwtDecode<Claims>(token);
}
